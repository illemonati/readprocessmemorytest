package tech.tioft.readprocessmemorytest

import com.sun.jna.Memory
import com.sun.jna.Native
import com.sun.jna.Pointer
import com.sun.jna.platform.win32.Kernel32
import com.sun.jna.platform.win32.User32
import com.sun.jna.platform.win32.WinNT
import com.sun.jna.ptr.IntByReference


val kernel32: Kernel32 = Native.load("kernel32", Kernel32::class.java)
val user32: User32 = Native.load("user32", User32::class.java)




fun main(args: Array<String>) {
    val baseAddress : Long = 0x7b2490
//    val pid = getPid("test.exe")
    val pid = 14304
    println("pid: $pid")
    val process : WinNT.HANDLE = kernel32.OpenProcess((WinNT.PROCESS_VM_READ or WinNT.PROCESS_VM_WRITE or WinNT.PROCESS_VM_OPERATION), false, pid) ?: return
    val basePointer = Pointer(baseAddress)
    val outBuffer = Memory(4)
    kernel32.ReadProcessMemory(process, basePointer, outBuffer, 4, null)
    println(outBuffer.getInt(0))

    val newPointer = Memory(4)
    newPointer.setInt(0, 0x64)
    val success = kernel32.WriteProcessMemory(process, basePointer, newPointer, 4, null)
    println(success)
}

fun getPid(window: String): Int {
    val pid = IntByReference(0)
    user32.GetWindowThreadProcessId(User32.INSTANCE.FindWindow(null, window), pid)
    return pid.value
}